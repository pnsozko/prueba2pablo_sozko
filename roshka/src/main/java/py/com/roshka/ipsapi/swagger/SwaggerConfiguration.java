package py.com.roshka.ipsapi.swagger;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;
/**
 * Clase de configuracion del swagger
 * @author psozko
 *
 */
@EnableSwagger2
@Configuration
@EnableAutoConfiguration
public class SwaggerConfiguration {
	
	@Bean
    public Docket usersApi() {
        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(usersApiInfo())
                .select()
                .paths(PathSelectors.any())
                .apis(RequestHandlerSelectors.basePackage("py.com.roshka.ipsapi.views"))
                .build()
                .useDefaultResponseMessages(false);
    }
	
	private ApiInfo usersApiInfo() {
		   Contact contact=new Contact("Roshka", "http://www.roshka.com.py/", "pablo@roshka.com");
	        return new ApiInfoBuilder()
	                .title("Api de Notificaciones")
	                .version("1.0")
	                .contact(contact)
	               // .license("Apache License Version 2.0")
	                .build();
	}
}
