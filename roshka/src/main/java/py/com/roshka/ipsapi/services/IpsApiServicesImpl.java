package py.com.roshka.ipsapi.services;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import py.com.roshka.ipsapi.beans.Asegurado;
import py.com.roshka.ipsapi.constant.IpsApiErrorsCode;
import py.com.roshka.ipsapi.dao.AseguradoDAO;
import py.com.roshka.ipsapi.exceptions.IpsApiErrorType;
import py.com.roshka.ipsapi.exceptions.IpsApiException;
import py.com.roshka.ipsapi.utils.IpsApiUtil;

@Service
public class IpsApiServicesImpl implements IpsApiServices {

	@Autowired
	AseguradoDAO aseguradoDAO;

	@Override
	public Asegurado getAsegurado(String nroCIC) throws IpsApiException {
		// TODO Auto-generated method stub
		
		Asegurado asegurado = aseguradoDAO.findPersona(nroCIC);
				if (!IpsApiUtil.isNumeric(nroCIC) ) {
					throw new IpsApiException(IpsApiErrorsCode.ASEGURADO_FORMATO_NO_VALIDO.getCodigo(),
							"Parámetros inválidos", IpsApiErrorType.NOT_FOUND);
				}
				if (asegurado == null) {			
					throw new IpsApiException(IpsApiErrorsCode.ASEGURADO_NO_ENCONTRADO.getCodigo(),
							"usuario con cédula "+nroCIC+" no existe", IpsApiErrorType.NOT_FOUND);
				}
		return asegurado;
	}
	
	
	
}
