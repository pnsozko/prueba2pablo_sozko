package py.com.roshka.ipsapi.beans;

import java.util.ArrayList;
import java.util.Date;

import io.swagger.annotations.ApiModelProperty;


public class Asegurado {
	@ApiModelProperty(value="Nro. de documento del asegurado")
	private String documento;
	@ApiModelProperty(value="Nombres del asegurado")
	private String nombres;
	@ApiModelProperty(value="Apellidos del asegurado")
	private String apellidos;
	@ApiModelProperty(value="Fecha de nacimineto del asegurado")
	private String fechaNacim;
	@ApiModelProperty(value="Sexo del asegurado")
	private String sexo;
	@ApiModelProperty(value="Tipo de asegurado")
	private String tipoAsegurado;
	@ApiModelProperty(value="Beneficiarios activos del asegurado")
	private Integer beneficiariosActivos;
	@ApiModelProperty(value="Asegurado enrolado")
	private String enrolado;
	@ApiModelProperty(value="Vencimiento de Fe de Vida")
	private String vencimientoFeDeVida;
	@ApiModelProperty(value="Lista de Empleadores del Asegurado")
	private ArrayList<Empleador> empleadores = new ArrayList<Empleador>();
	public String getDocumento() {
		return documento;
	}
	public void setDocumento(String documento) {
		this.documento = documento;
	}
	public String getNombres() {
		return nombres;
	}
	public void setNombres(String nombres) {
		this.nombres = nombres;
	}
	public String getApellidos() {
		return apellidos;
	}
	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}
	public String getFechaNacim() {
		return fechaNacim;
	}
	public void setFechaNacim(String fechaNacim) {
		this.fechaNacim = fechaNacim;
	}
	public String getSexo() {
		return sexo;
	}
	public void setSexo(String sexo) {
		this.sexo = sexo;
	}
	public String getTipoAsegurado() {
		return tipoAsegurado;
	}
	public void setTipoAsegurado(String tipoAsegurado) {
		this.tipoAsegurado = tipoAsegurado;
	}
	public Integer getBeneficiariosActivos() {
		return beneficiariosActivos;
	}
	public void setBeneficiariosActivos(Integer beneficiariosActivos) {
		this.beneficiariosActivos = beneficiariosActivos;
	}
	public String getEnrolado() {
		return enrolado;
	}
	public void setEnrolado(String enrolado) {
		this.enrolado = enrolado;
	}
	public String getVencimientoFeDeVida() {
		return vencimientoFeDeVida;
	}
	public void setVencimientoFeDeVida(String vencimientoFeDeVida) {
		this.vencimientoFeDeVida = vencimientoFeDeVida;
	}
	public ArrayList<Empleador> getEmpleadores() {
		return empleadores;
	}
	public void setEmpleadores(ArrayList<Empleador> empleadores) {
		this.empleadores = empleadores;
	}
	
	
	
}
