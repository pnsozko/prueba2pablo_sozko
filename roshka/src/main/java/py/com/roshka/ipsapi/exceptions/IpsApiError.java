package py.com.roshka.ipsapi.exceptions;

public class IpsApiError {
	 private String codigo;
	    private String error;

	    public IpsApiError() {
	    }

	    public String getCodigo() {
	        return this.codigo;
	    }

	    public void setCodigo(String codigo) {
	        this.codigo = codigo;
	    }

	    public String getError() {
	        return this.error;
	    }

	    public void setError(String error) {
	        this.error = error;
	    }
	    
	    public String toString() {
	        return "EnursingError [codigo=" + this.codigo + ", error=" + this.error + "]";
	    }

}
