package py.com.roshka.ipsapi.services;

import java.io.IOException;

import py.com.roshka.ipsapi.beans.Asegurado;
import py.com.roshka.ipsapi.exceptions.IpsApiException;

public interface IpsApiServices  {
	public Asegurado getAsegurado(String nroCIC) throws IpsApiException;

}
