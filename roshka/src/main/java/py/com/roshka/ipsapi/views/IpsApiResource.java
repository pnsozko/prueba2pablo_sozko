package py.com.roshka.ipsapi.views;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import py.com.roshka.ipsapi.beans.Asegurado;
import py.com.roshka.ipsapi.exceptions.IpsApiException;
import py.com.roshka.ipsapi.services.IpsApiServices;

/**
 * Clase que apificara la web de consultas de asegurados del IPS
 * @author psozko
 *
 */

@RestController
@Api(value= "Api api para consultas de asegurados el Instituto de Prevision Social " )
@RequestMapping("/consulta")
public class IpsApiResource {
	
	@Autowired
	IpsApiServices ipsApiServices;
	
	@ApiOperation(value = "Get Asegurado IPS",notes = "Traer datos de un asegurado en ips")
    @ResponseStatus(value = HttpStatus.OK)
    @RequestMapping(value = "/{nro_cic}", method = RequestMethod.GET)
	public Asegurado getAseguradosIps(
			@ApiParam(value = "Nro. de Documento")
			@PathVariable(value = "nro_cic", required = true) String nroCic) throws IpsApiException {
		return ipsApiServices.getAsegurado(nroCic); 
	}
	
}
