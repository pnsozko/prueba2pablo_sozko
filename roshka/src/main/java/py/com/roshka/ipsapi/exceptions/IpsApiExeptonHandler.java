package py.com.roshka.ipsapi.exceptions;

import javax.servlet.http.HttpServletResponse;

import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import py.com.roshka.ipsapi.constant.IpsApiErrorsCode;

@ControllerAdvice
public class IpsApiExeptonHandler {
	@ExceptionHandler(IpsApiException.class)
    public @ResponseBody IpsApiError handleNotFoundException(HttpServletResponse request, IpsApiException ex) {
		IpsApiError error= new IpsApiError();
        error.setCodigo(ex.getCode());
        error.setError(ex.getErrorMessage());
        switch (ex.getErrorType()) {
            case NOT_FOUND:
                request.setStatus(HttpServletResponse.SC_NOT_FOUND);
                break;
            case UNAUTHORIZED:
                request.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
                break;
            case BAD_REQUEST:
                request.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                break;
            case SEE_OTHER:
                request.setStatus(HttpServletResponse.SC_SEE_OTHER);
                break;
            case CONFLICT:
                request.setStatus(HttpServletResponse.SC_CONFLICT);
                break;
            case INTERNAL_SERVER_ERROR:
                request.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
                break;
            default:
                break;
        }
        return error;
    }
    

    @ExceptionHandler(Exception.class)
    public @ResponseBody IpsApiError handleGenericException(HttpServletResponse request, Exception ex) {
        IpsApiError error= new IpsApiError();
        error.setCodigo(IpsApiErrorsCode.ASEGURADO_ERROR_INESPERADO.getCodigo());
        error.setError(ex.getMessage()+ "Error interno del servidor");
        request.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
        return error;
    }
    @ExceptionHandler(HttpRequestMethodNotSupportedException.class)
    public @ResponseBody IpsApiError handleGenericException(HttpServletResponse request, HttpRequestMethodNotSupportedException ex) {
        IpsApiError error= new IpsApiError();
        error.setCodigo(IpsApiErrorsCode.ASEGURADO_ERROR_INESPERADO.getCodigo() + "La operación que desea realizar es inválida");
        error.setError(ex.getMessage());
        request.setStatus(HttpServletResponse.SC_METHOD_NOT_ALLOWED);
        return error;
    }

}
