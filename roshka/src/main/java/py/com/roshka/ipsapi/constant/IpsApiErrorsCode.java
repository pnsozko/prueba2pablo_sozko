package py.com.roshka.ipsapi.constant;

public enum IpsApiErrorsCode {
	ASEGURADO_NO_ENCONTRADO("g100","El asegurado no existe"),
    ASEGURADO_FORMATO_NO_VALIDO("g101","Parámetros inválidos"),
    ASEGURADO_ERROR_INESPERADO("g102", "Error interno del servidor");
    
    private String codigo;
    private String mensaje;
    private IpsApiErrorsCode(String codigo, String mensaje) {
        this.codigo = codigo;
        this.mensaje = mensaje;
    }
    public String getCodigo(){
        return codigo;
    }
    public String getMensaje(){
        return mensaje;
    }
}
