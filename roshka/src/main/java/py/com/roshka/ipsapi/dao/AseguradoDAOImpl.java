package py.com.roshka.ipsapi.dao;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.stereotype.Repository;

import py.com.roshka.ipsapi.beans.Asegurado;
import py.com.roshka.ipsapi.beans.Empleador;
import py.com.roshka.ipsapi.exceptions.IpsApiException;
import py.com.roshka.ipsapi.utils.IpsApiUtil;

@Repository
public class AseguradoDAOImpl implements AseguradoDAO {

	@Override
	public Asegurado findPersona(String nroCic)  {
		
		 HashMap<Integer,String> titulos = new HashMap<Integer,String>();
		 HashMap<Integer,String> valores = new HashMap<Integer,String>();
		 HashMap<Integer,Empleador> filasValoresE = new HashMap<Integer,Empleador>();
		 		
		Asegurado asegurado = null;
		
		// TODO Auto-generated method stub
		try {
			Connection.Response response = Jsoup.connect("http://servicios.ips.gov.py/consulta_asegurado/comprobacion_de_derecho_externo.php")
					 .userAgent("Mozilla/5.0")
	                 .timeout(10 * 1000)
	                 .data("nro_cic", nroCic)
	                 .data("envio", "ok")
					.method(Connection.Method.POST)
					.followRedirects(true)
					.execute();
			
			Document document = response.parse();
			Elements form = document.select("form");
			Elements tableA = document.select("table:contains(Nombres)");
			Elements titulosTh = tableA.select("th");
			Elements valoresTd = tableA.select("td");
			Elements tableE = document.select("table:contains(Nro. Patronal)");
			
			/*PSOZKO: al no tener control sobre la web expuesta por ips a modo de prevenir alguna actualizacion con el orden de los campos
			expuestos por IPS cargo tanto los titulos como los valores de las tablas en un Hashmap a modo de luego con una condicion 
			poder verificar que valor corresponde a que campo */
						
			titulos = IpsApiUtil.elementsToHashmapString(titulosTh);
			valores = IpsApiUtil.elementsToHashmapString(valoresTd);			
			
			if (valoresTd.size() > 0) {
				asegurado = new Asegurado();
			
				//PSOZKO: recorro los campos y asigno al asegurado los valores a sus atributos 
			
				for(Integer i : titulos.keySet()) {
					if (titulos.get(i).toUpperCase().equals("NRO DOCUMENTO")) {
						asegurado.setDocumento(valores.get(i));
					};
				
					if (titulos.get(i).toUpperCase().equals("NOMBRES")) {
						asegurado.setNombres(valores.get(i));
					};
					if (titulos.get(i).toUpperCase().equals("APELLIDOS")) {
						asegurado.setApellidos(valores.get(i));
					};
					if (titulos.get(i).toUpperCase().equals("FECHA NACIM")) {
						asegurado.setFechaNacim(valores.get(i));
					};
					if (titulos.get(i).toUpperCase().equals("SEXO")) {
						asegurado.setSexo(valores.get(i));
					};
					if (titulos.get(i).toUpperCase().equals("TIPO ASEG.")) {
						asegurado.setTipoAsegurado(valores.get(i));
					};
					if (titulos.get(i).toUpperCase().equals("BENEFICIARIOS ACTIVOS")) {
						asegurado.setBeneficiariosActivos(Integer.parseInt( valores.get(i)));
					};
					if (titulos.get(i).toUpperCase().equals("ENROLADO")) {
						asegurado.setEnrolado(valores.get(i));
					};
					if (titulos.get(i).toUpperCase().equals("VENCIMIENTO DE FE DE VIDA")) {
						asegurado.setVencimientoFeDeVida(valores.get(i));
					};				
				}
				//PSOZKO: Estiro los datos del empleador o los empleadores
				
				Elements valoresTrE = tableE.select("tr");
				
				filasValoresE = IpsApiUtil.elementsToHashmapEmpleador(valoresTrE);
				
				if (filasValoresE.size() > 0 ) {
					for (Integer i : filasValoresE.keySet() ) {
						asegurado.getEmpleadores().add(filasValoresE.get(i));
					}					
				}
			}	
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}	
		
		return asegurado;
		
	}

}
