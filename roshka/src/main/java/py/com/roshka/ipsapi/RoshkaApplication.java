package py.com.roshka.ipsapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;

@SpringBootApplication()
public class RoshkaApplication {

	public static void main(String[] args) {
		SpringApplication.run(RoshkaApplication.class, args);
	}

}
