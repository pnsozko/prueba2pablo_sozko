package py.com.roshka.ipsapi.dao;

import java.io.IOException;

import org.springframework.stereotype.Repository;

import py.com.roshka.ipsapi.beans.Asegurado;
@Repository
public interface AseguradoDAO  {
	
	public Asegurado findPersona(String nroCic);
}
