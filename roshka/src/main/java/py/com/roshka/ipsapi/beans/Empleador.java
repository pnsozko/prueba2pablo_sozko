package py.com.roshka.ipsapi.beans;

import java.sql.Date;



public class Empleador {
	private String nroPatronal;
	private String empleador;
	private String estado;
	private Integer mesesDeAporte;
	private String vencimiento;
	private String ultimoPeriodoAbonado;
	public String getNroPatronal() {
		return nroPatronal;
	}
	public void setNroPatronal(String nroPatronal) {
		this.nroPatronal = nroPatronal;
	}
	public String getEmpleador() {
		return empleador;
	}
	public void setEmpleador(String empleador) {
		this.empleador = empleador;
	}
	public String getEstado() {
		return estado;
	}
	public void setEstado(String estado) {
		this.estado = estado;
	}
	public Integer getMesesDeAporte() {
		return mesesDeAporte;
	}
	public void setMesesDeAporte(Integer mesesDeAporte) {
		this.mesesDeAporte = mesesDeAporte;
	}
	public String getVencimiento() {
		return vencimiento;
	}
	public void setVencimiento(String vencimiento) {
		this.vencimiento = vencimiento;
	}
	public String getUltimoPeriodoAbonado() {
		return ultimoPeriodoAbonado;
	}
	public void setUltimoPeriodoAbonado(String ultimoPeriodoAbonado) {
		this.ultimoPeriodoAbonado = ultimoPeriodoAbonado;
	}
	
	
	
}
