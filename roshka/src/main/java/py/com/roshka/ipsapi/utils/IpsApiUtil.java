package py.com.roshka.ipsapi.utils;

import java.util.HashMap;

import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import py.com.roshka.ipsapi.beans.Empleador;

public class IpsApiUtil {
	public static HashMap<Integer, String> elementsToHashmapString(Elements elements ){
		HashMap<Integer,String> hashMap = new HashMap<Integer,String>();
		Integer i = 0;
		for (Element element : elements) {
			hashMap.put(i++, element.text());
		}
		
		
		return hashMap;
	}
	
	public static HashMap<Integer, Empleador> elementsToHashmapEmpleador(Elements elements ){
		
		HashMap<Integer,String> titulosE = new HashMap<Integer,String>();
		HashMap<Integer,String> valoresE = new HashMap<Integer,String>();
		
		HashMap<Integer,Empleador> hashMap = new HashMap<Integer,Empleador>();
		Integer i = 0;
		
		Elements titulosThE = elements.select("th");
		titulosE = IpsApiUtil.elementsToHashmapString(titulosThE);
				
		for (Element element : elements) {
			
			Empleador empleador = null;
			
			Elements valoresTdE = element.select("td");
			
			valoresE = IpsApiUtil.elementsToHashmapString(valoresTdE);
			
			
			if (valoresE.size() > 0) {
				empleador = new Empleador();
				
				for(Integer j : titulosE.keySet()) {
					if (titulosE.get(j).toUpperCase().equals("NRO. PATRONAL")) {
						empleador.setNroPatronal(valoresE.get(j));
					};
				
					if (titulosE.get(j).toUpperCase().equals("EMPLEADOR")) {
						empleador.setEmpleador(valoresE.get(j));
					};
					if (titulosE.get(j).toUpperCase().equals("ESTADO")) {
						empleador.setEstado(valoresE.get(j));
					};
					if (titulosE.get(j).toUpperCase().equals("MESES DE APORTE")) {
						if (valoresE.get(j) != null) {
							empleador.setMesesDeAporte(Integer.parseInt(valoresE.get(j)));
						}	
					};
					if (titulosE.get(j).toUpperCase().equals("VENCIMIENTO")) {
						empleador.setVencimiento(valoresE.get(j));
					};
					if (titulosE.get(j).toUpperCase().equals("ULTIMO PERIODO ABONADO")) {
						empleador.setUltimoPeriodoAbonado(valoresE.get(j));
					};
				
				}
				
				if (empleador != null) {
					hashMap.put(i++, empleador);
				}	
			}	
		}
		
		
		return hashMap;
	}
	
	public static boolean isNumeric(String cadena){
		try {
			Integer.parseInt(cadena);
			return true;
		} catch (NumberFormatException nfe){
			return false;
		}
	}
}
