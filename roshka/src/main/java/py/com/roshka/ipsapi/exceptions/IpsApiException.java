package py.com.roshka.ipsapi.exceptions;

import java.util.Date;

public class IpsApiException extends Exception {
	
	private static final long serialVersionUID = -6734677800096511201L;
    private String code;
    private Long referenceCode;
    private String errorMessage;
    private String userMessage;
    private IpsApiErrorType errorType;

    public IpsApiException(String code, String errorMessage, IpsApiErrorType errorType) {
        this.code = code;
        this.referenceCode = this.buildReferenceCode();
        this.errorMessage = errorMessage;
        this.userMessage = userMessage;
        this.errorType = errorType;
    }

    public IpsApiException(String code, String errorMessage,  IpsApiErrorType errorType, Exception ex) {
        this.code = code;
        this.referenceCode = this.buildReferenceCode();
        this.errorMessage = errorMessage;
        this.userMessage = userMessage;
        this.errorType = errorType;
        super.initCause(ex.getCause());
    }

    public Long buildReferenceCode() {
        Date now = new Date();
        return now.getTime();
    }

    public String getCode() {
        return this.code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Long getReferenceCode() {
        return this.referenceCode;
    }

    public void setReferenceCode(Long referenceCode) {
        this.referenceCode = referenceCode;
    }

    public String getErrorMessage() {
        return this.errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public String getUserMessage() {
        return this.userMessage;
    }

    public void setUserMessage(String userMessage) {
        this.userMessage = userMessage;
    }

    public IpsApiErrorType getErrorType() {
        return this.errorType;
    }

    public void setErrorType(IpsApiErrorType errorType) {
        this.errorType = errorType;
    }
}
